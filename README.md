# docker-nagios

* Source: https://bitbucket.org/amelon/
* Nagios installation guide: https://www.howtoforge.com/tutorial/how-to-install-nagios-on-ubuntu-2004/

## Configure Dockerfile

Edit `Dockerfile`:

* Change `SITE_FQDN` to the host machine's FQDN hostname
* Change `MAIL_RELAY_HOST` to the site's mail relay

## Manage Nagios Docker container

Build the Docker image:

    $ docker-compose build

Bring up the Docker container:

    $ docker-compose up -d

Verify that the container is up and running (State is Up):

    $ docker-compose ps

Enter the container's shell:

    $ docker-compose exec nagios bash

Bring down the comtainer:

    $ docker-compose down

## Install NRPE service on the Nagios client (host being monitored)

    $ sudo apt update
    $ sudo apt install nagios-nrpe-server monitoring-plugins


Edit `/etc/nagios/nrpe_local.cfg` and add:

    #server_address=172.16.0.6    <--- Client IP address, add it if client has more than one active interface
    allowed_hosts=127.0.0.1,::1,172.16.0.5   <--- Nagios server IP address

    command[check_users]=/usr/lib/nagios/plugins/check_users -w 5 -c 10
    command[check_root_disk]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -p /
    command[check_all_disks]=/usr/lib/nagios/plugins/check_disk -w 20% -c 10% -e -A -i /run -i /var/lib/docker -i /snap/core
    command[check_ping]=/usr/lib/nagios/plugins/check_ping -H 172.16.0.6 -w 100.0,20% -c 500.0,60% -p 5
    command[check_ssh]=/usr/lib/nagios/plugins/check_ssh -4 172.16.0.6
    command[check_http]=/usr/lib/nagios/plugins/check_http -I 172.16.0.6

And restart NRPE service:

    $ systemctl restart nagios-nrpe-server
    $ systemctl enable nagios-nrpe-server

Test NRPE service:

    $ sudo -H -u nagios bash -c "/usr/lib/nagios/plugins/check_users -w 5 -c 10"

## Add Nagios client configuration on Nagios server

On Nagios server, check client's NRPE service:

    $ /usr/lib/nagios/plugins/check_nrpe -H 172.16.0.6
    $ /usr/lib/nagios/plugins/check_nrpe -H 172.16.0.6 -c check_ping

Add client config on Nagios server. Edit /usr/local/nagios/etc/servers/client01.cfg:

    define host {
            use                          linux-server
            host_name                    client01
            alias                        Ubuntu Host
            address                      172.16.0.6
            register                     1
    }
    
    define service {
          host_name                       client01
          service_description             PING
          check_command                   check_nrpe!check_ping
          max_check_attempts              2
          check_interval                  2
          retry_interval                  2
          check_period                    24x7
          check_freshness                 1
          contact_groups                  admins
          notification_interval           2
          notification_period             24x7
          notifications_enabled           1
          register                        1
    }
    
    define service {
          host_name                       client01
          service_description             Check Users
          check_command                   check_nrpe!check_users
          max_check_attempts              2
          check_interval                  2
          retry_interval                  2
          check_period                    24x7
          check_freshness                 1
          contact_groups                  admins
          notification_interval           2
          notification_period             24x7
          notifications_enabled           1
          register                        1
    }
    
    define service {
          host_name                       client01
          service_description             Check SSH
          check_command                   check_nrpe!check_ssh
          max_check_attempts              2
          check_interval                  2
          retry_interval                  2
          check_period                    24x7
          check_freshness                 1
          contact_groups                  admins
          notification_interval           2
          notification_period             24x7
          notifications_enabled           1
          register                        1
    }
    
    define service {
          host_name                       client01
          service_description             Check Root / Disk
          check_command                   check_nrpe!check_root
          max_check_attempts              2
          check_interval                  2
          retry_interval                  2
          check_period                    24x7
          check_freshness                 1
          contact_groups                  admins
          notification_interval           2
          notification_period             24x7
          notifications_enabled           1
          register                        1
    }
    
    define service {
          host_name                       client01
          service_description             Check APT Update
          check_command                   check_nrpe!check_apt
          max_check_attempts              2
          check_interval                  2
          retry_interval                  2
          check_period                    24x7
          check_freshness                 1
          contact_groups                  admins
          notification_interval           2
          notification_period             24x7
          notifications_enabled           1
          register                        1
    }
    
    define service {
          host_name                       client01
          service_description             Check HTTP
          check_command                   check_nrpe!check_http
          max_check_attempts              2
          check_interval                  2
          retry_interval                  2
          check_period                    24x7
          check_freshness                 1
          contact_groups                  admins
          notification_interval           2
          notification_period             24x7
          notifications_enabled           1
          register                        1
    }

Verify Nagios configs:

    $ nagios -v /usr/local/nagios/etc/nagios.cfg

Restart Nagios server:

    $ systemctl restart nagios

Nagios server URL: http://172.16.0.5/nagios/  (login: `nagiosadmin`, password: `admin`)

## Miscellaneous notes

* Various Nagios logs are found under `/usr/local/nagios/var/`.

