FROM ubuntu:20.04
MAINTAINER Vui Le "amelon@gmail.com"

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \ 
      vim \
      build-essential \
      curl \
      sudo \
      jq \
      tzdata \
      iputils-ping \
      dnsutils 

# Nagios dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autoconf bc gawk dc gcc libc6 make wget unzip apache2 \
    php libapache2-mod-php libgd-dev libmcrypt-dev make \
    libssl-dev snmp libnet-snmp-perl gettext

RUN cd ~/ \
    && wget https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.6.tar.gz \
    && tar -xf nagios-4.4.6.tar.gz \
    && cd nagioscore-*/ \
    && ./configure --with-httpd-conf=/etc/apache2/sites-enabled \
    && make all \
    && make install-groups-users \
    && usermod -a -G nagios www-data \
    && make install \
    && make install-daemoninit \
    && make install-commandmode \
    && make install-config \
    && make install-webconf \
    && a2enmod rewrite cgi \
    && rm -f nagios-4.4.6.tar.gz \
    && apt-get install -y monitoring-plugins nagios-nrpe-plugin

RUN mv /etc/localtime /etc/localtime.old \
    && ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

RUN mkdir -p /code /configs /usr/local/nagios/etc/servers
COPY code/heartbeat /code
COPY configs /configs

USER root

ENV MAIL_RELAY_HOST     smtp.amelon.com
ENV SITE_FQDN           amelon.com

RUN echo postfix postfix/main_mailer_type string "'Internet Site'" | debconf-set-selections \
 && echo postfix postfix/mailname string ${SITE_FQDN} | debconf-set-selections \
 && apt-get install -y postfix mailutils bsd-mailx \
 && mv /etc/postfix/main.cf /etc/postfix/main.cf.orig \
 && sed "/^relayhost =/ s/$/[$MAIL_RELAY_HOST]:587/" /etc/postfix/main.cf.orig > /etc/postfix/main.cf

RUN mv /var/www/html/index.html /var/www/html/index.html.orig \
 && echo '<meta http-equiv="refresh" content="1; URL=/nagios" />' > /var/www/html/index.html

